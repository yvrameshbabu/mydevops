****A server pinging app with Golang and Docker****

_This is an application named Pinger build with Golang in Containarisation orchestration._

**Key Points.**

Docker containarisation is being used for installing this app and with this, we can create multiple containers with single command and allow them to communicate each other in a private network.

The gitlab CI-CD allows the application enables this project in continious integration and delivery for multiple servers as soon as developer push the code in Git.

> Note: This application is built and tested in Windows Docker desktop. 

**Tools Used:**

> go to run the application (check with go version)
> docker for image building/publishing (check with docker version)
> docker-compose for environment provisioning (check with docker-compose version)
> git for source control (check with git -v)
> make for simple convenience scripts (check with make -v)

**Pre-requisites:**

Git software.
Windows/Linux with Docker installed.
Git-lab-Runner installed and sync with Gitlab to run the CI-CD. Can be run with shared runners.

**Execution Steps:**

- clone this repository.
.gitlab-ci.yml file located in root directory is the CI-CD job to be executed in gitlab for any changes push into repository.
This file contains 3 stages. 1. Build 2.Test 3.Deploy.
Since this is intendeded for a sample project, Test and Deploy phases were mrked for just testing purpose.
The build phase consists of 
- Installation of Make and Docker-compose
- Creating Docker Image for Pinger app
- create pinger.tar with docker commands to create  for artifacts preparation
As a post scripts, to accomdate testing in the same environment, I have used Post scripts for below tasks.

- untar the pingar.tar with docker
- deploy  the docker Image with Docker-compose
- The docker-compose will create two pinger containers. The build_mypinger1_1 and build_mypinger2_1 will communicate through the default docker bridge network.
- These containers are mounted with port 8080 and with equivalent host port as 8001 and 8002 accordingly.

- In the last, the tar file will be uploaded into gitlab artifacts and retained for 1 week.

- Test and Deploy phases will execute in dry run with simple echo commands to ensure job can be succeded if those phases configured.

> Running containers from Tar file.

- The pinger.tar downloaded from gitlab artifacts,  can be used directly to pull (create)  the image and ready for running.
to untar: Execute below command
-docker load -i ingepinger.tar

> Development mode

As standalone development mode, this project can be executed with below commands.

go to the project directory in cmd prompt post clone this project, execute below commands.

> make docker_goimage
- This will create a docker image named mydevops/pinger.

> make docker_gotest
- This will start the containers and enable the port mapping 8000:8000

> make docker_gotar
- This will create a tar file for docker Image. The same can be used in any other location by untarring the image with below command 
> make docker_gountar

- The docker-compose file helps to do the above tasks for multiple containers build and run. To test the same,execute the below command.
> make docker_deploy















